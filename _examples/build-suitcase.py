#!/usr/bin/env python3
import sys
from suitcaser.suitcasectl import Suitcase
import logging

def main():
    if len(sys.argv) != 2:
            logging.fatal(f"usage: {sys.argv[0]} TARGET")
            return 2
    logging.basicConfig(level=logging.INFO)
    logging.info("Starting run")
    sc = Suitcase()

    sc.create(sys.argv[1])
    pass

if __name__ == "__main__":
    sys.exit(main())