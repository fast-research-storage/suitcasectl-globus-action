#!/bin/bash
set -e
set -x

## Set up some defaults so we can run this outside of a container that may
## already have these set
NUM_WORKERS="${NUM_WORKERS:=1}"
ALLOWED_IPS="${ALLOWED_IPS:=localhost}"

# Build up array of args
gargs=(
  "--bind=0.0.0.0:8000"
  "--name=suitcaser"
  "--workers=${NUM_WORKERS}"
  "--forwarded-allow-ips=${ALLOWED_IPS}"
  "--worker-class=gevent"
  "--keep-alive=0"
  "--capture-output"
  "--reuse-port"
)

# If we have been given additional args, inject them here
if [ "${GUNICORN_ADDITIONAL_ARGS}" != "" ]; then
  gargs+=("${GUNICORN_ADDITIONAL_ARGS}")
fi

# Final arg must be the create/app command
gargs+=("suitcaser:create_app()")

# Runit!
gunicorn "${gargs[@]}"