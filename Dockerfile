FROM pypy:3.9-bullseye
# FROM python:3.11-alpine

WORKDIR /usr/src/suitcasectl-globus-action
ENV PYTHONPATH=/usr/src/suitcasectl-globus-action \
    DEBIAN_FRONTEND=noninteractive \
    GUNICORN_USER=nobody \
    GUNICORN_GROUP=nogroup \
    NUM_WORKERS=4 \
    ALLOWED_IPS=localhost \
    TZ=America/New_York

COPY . .

SHELL ["/bin/bash", "-o", "pipefail", "-c"]
# hadolint ignore=DL3018,DL3008
RUN echo "deb [signed-by=/etc/apt/trusted.gpg.d/devil-ops-debs.gpg] https://oneget.oit.duke.edu/ devil-ops-debs main" > /etc/apt/sources.list.d/devil-ops-debs.list && \
    wget -qO- https://oneget.oit.duke.edu/debian-feeds/devil-ops-debs.pub | gpg --dearmor -o /etc/apt/trusted.gpg.d/devil-ops-debs.gpg && \
    apt-get update && \
    apt-get install -y --no-install-recommends git suitcasectl && \
    rm -rf /var/lib/apt/lists/* && \
    pip install --no-cache-dir -r requirements.txt

#USER ${GUNICORN_USER}
ENTRYPOINT ["./entrypoint.sh"]