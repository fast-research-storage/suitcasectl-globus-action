# Save for putting suitcasectl python mods perhaps?
import logging
import shutil
import subprocess

class Suitcase(object):

    def __init__(self):
        logging.debug("Creating new suitcaser")
        self.suitcasectl_bin = shutil.which("suitcasectl")
        if not self.suitcasectl_bin:
            logging.fatal("could not find suitcasectl binary")

    def create(self, target: str) -> bool:
        logging.debug(f"Creating new suitcase on {target}")
        status = subprocess.run([self.suitcasectl_bin, "create", "suitcase", target], capture_output=True)
        logging.info(f"Completed run and got {status}")
