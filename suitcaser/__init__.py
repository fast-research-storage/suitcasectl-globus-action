from flask import Flask
import os
import logging
from suitcaser import config
from suitcaser.blueprint import aptb
from globus_action_provider_tools.flask.helpers import assign_json_provider

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__)
    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_object(config)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)
    
    assign_json_provider(app)
    app.logger.setLevel(logging.DEBUG)
    app.register_blueprint(aptb)
    return app

if __name__ == "__main__":
    app = create_app()
    app.run(debug=True)