# SuitcaseCTL Globus Action

Provides an endpoint for Globus that runs SuitcaseCTL

## Running in Docker

You can run with docker using:

```bash
$ docker run gitlab-registry.oit.duke.edu/fast-research-storage/suitcasectl-globus-action:$TAG
...
```

Use whichever tag is appropriate (`v0.1.0`, `v0.2.0`, etc).

To build a new container, tag using the semver standard (example: v1.2.3)

## Running in Podman (with TLS/ACME via Caddy)

```bash
$ podman network create shared
...
$ podman run \
  --network shared \
  --name suitcaser \
  -p 127.0.0.1:8000:8000 \
  gitlab-registry.oit.duke.edu/fast-research-storage/suitcasectl-globus-action:v0.3.0
...
$ podman run \
  --network shared \
  --name caddy \
  -p 80:80 \
  -p 443:443 \
  -v /srv/caddy/config/Caddyfile:/etc/caddy/Caddyfile \
  -v /srv/caddy/data/:/data:Z \
  -v /srv/caddy/config/:/config:Z \
  docker.io/library/caddy:latest
...
```

The Caddyfile above looks like this:

```plain
## This first line should be the fqdn that will be in the cert request
your-fqdn.example.edu {
        tls your-email@example.edu {
                ca https://locksmith.oit.duke.edu/acme/v2/directory
        }
        reverse_proxy http://suitcaser:8000
}
```

## Oddities?

The `globus_action_provider_tools` in pip doesn't work with the example code, so
I installed from the main branch with:

```bash
$ pip install git+https://github.com/globus/action-provider-tools.git@main
...
```

Generating the `requirements.txt` with `pip freeze` seems to lock this to a
specific commit, so I _think_ that's what we want 🤨

## Docs

- [Globus SDK Python](https://globus-sdk-python.readthedocs.io/en/stable/)
- [Action Provider](https://action-provider-tools.readthedocs.io/en/latest/)
- [Examples](https://action-provider-tools.readthedocs.io/en/latest/examples.html)
